import UIKit

let string = "I going, to split this string"
print(string.split(separator: " "))

class Student {
    
    private static var counter = 0
    
    private var id: Int
    let name: String
    let secondName: String
    var age: Int
    
    init(name: String, secondName: String, age: Int) {
        self.name = name
        self.secondName = secondName
        self.age = age
        
        Student.counter += 1
        self.id = Student.counter
    }
    
    func getStudentID() -> Int {
        return id
    }
    static func whoIsStudent() {
        print("Студент - это учащийся высшего учебного заведения или техникума.")
    }
    
    func aboutStudentInfo() {
        print("Меня зовут \(name) мне \(age)")
    }
}

let student = Student(name: "Вася", secondName: "Печкин", age: 15)
let student2 = Student(name: "Антон", secondName: "Жирнич", age: 17)

student.aboutStudentInfo()
print(student.getStudentID())
print(student2.getStudentID())

Student.whoIsStudent()


class StudentManager {
    public static let shared = StudentManager()
    
    private var students: [Student] = []
    
    func getAllStudents() -> [Student] {
        return students
    }
    
    func addStudent(student: Student) {
        students.append(student)
    }
    
    func removeStudent(studentID: Int) {
        students.removeAll(where: {$0.getStudentID() == studentID})
    }
}



StudentManager.shared.addStudent(student: Student(name: "petuyh", secondName: "lohivoch", age: 14))
StudentManager.shared.addStudent(student: Student(name: "petuyh", secondName: "lohivoch", age: 321))
StudentManager.shared.addStudent(student: Student(name: "petuyh", secondName: "lohivoch", age: 123))
var students = StudentManager.shared.getAllStudents()
//students.forEach({$0.aboutStudentInfo()})

StudentManager.shared.removeStudent(studentID: students.first!.getStudentID())

students = StudentManager.shared.getAllStudents()
students.forEach({$0.aboutStudentInfo()})


struct StudentStruct {
    let name: String
    let secondName: String

    init(name: String, secondName: String) {
        self.name = name
        self.secondName = secondName
    }
}


struct StudentStruct2 {
    var name: String
    let secondName: String
}


//let studentStruct = StudentStruct(name: "Паха", secondName: "Чёлка")
let studentStruct2 = StudentStruct2(name: "Ваня", secondName: "Чиж")

var newStudentStruct2 = studentStruct2
newStudentStruct2.name = "Басалыга"
print(studentStruct2)
print(newStudentStruct2)

//let studentTime = student
//
//print(student.age)
//studentTime.age = 123123123
//print(student.age)
//print(studentTime.age)

//Создайте класс Car у которого будут следующие поля: модель, скорость, вес, цена, расход по километражу. Все поля кроме модели задать рандомно, можно использовать класс Int.random.  Создать 4 машины и найти какая среди машин будет самая быстрая, самая дешёвая. Дать возможность юзеру назначить дистанцию(совершить гонку) и получить среди всех машин ту которая проедет бысстрее всех(учитывая все их характеристики), ту которая проедет медленнее всех, ту у которой первой закончится бензин. Показать список после гонки в формате 1) место - и тд. Результат продемонстрировать в консоли.

//Создать структуру Человек с полями сила левой руки, сила правой руки, сила левой ноги, сила правой ноги, cила удара головой(быкана). Создать 20 человек, устроить драку таким образом: сначало первый человек дерётся со вторым, если побеждает то с третьим и тд, в случае если проигрывает то победивший продолжает путь и сражается со следующим. Характеристики задавать рандомно через рендж. Правила боя:

//Всего делается 3 удара по очереди, сначало бьет один боец, потом второй и тд по очереди 3 раза. Побеждает тот кто нанёс больше урона(усложнённо, сделать приоритет: голова самое большое количество урона, и тд). Результат продемонстрировать в консоли.
